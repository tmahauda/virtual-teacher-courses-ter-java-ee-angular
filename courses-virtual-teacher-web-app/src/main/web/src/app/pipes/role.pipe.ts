import { Pipe, PipeTransform } from '@angular/core';
import { Role } from '../models/role.model';
import { ERole } from '../models/e-role.enum';

/**
 * Pipe qui permet de convertir une liste de roles en une string
 */
@Pipe({
  name: 'role'
})
export class RolePipe implements PipeTransform {

  public transform(roles : Role[]): string {
    return roles.map(r => {
      if(r.name == ERole.ROLE_ADMIN) return "Administrateur";
      else if(r.name == ERole.ROLE_TEACHER) return "Enseignant";
      else if(r.name == ERole.ROLE_STUDENT) return "Etudiant";
      else return r.name;
    }).join(', ');
  }
}
