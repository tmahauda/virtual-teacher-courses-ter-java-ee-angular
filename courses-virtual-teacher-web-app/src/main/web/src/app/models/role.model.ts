import { Resource } from './resource.model';
import { ERole } from './e-role.enum';
import { Serializable } from './serializable.model';

export class Role extends Resource implements Serializable<Role> {
	public name: ERole;

    /**
     * Convert json to role object
     * @param json 
     */
	public fromJson(json: any): Role {
        console.log("json role", json);
         if(json == null) return null;

        var role = new Role();
		Object.assign(role, json);
		role.name = json.name ? ERole[json.name as string] : null;

        console.log("object role", role);

        return role;
    }

    /**
     * Convert role object to json
     * @param role 
     */
    public toJson(role: Role): any {
        return JSON.stringify(role);
    }
}
