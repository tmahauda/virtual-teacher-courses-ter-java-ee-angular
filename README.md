# Virtual Teacher Courses

<div align="center">
<img width="500" height="400" src="vtc.png">
</div>

## Description du projet

Application web réalisée avec Java EE coté serveur et Angular coté client en MASTER INFO 1 à l'université d'Angers dans le cadre du module "TER" durant l'année 2019-2020 avec un groupe de dix personnes. \
Cette application permet à tout enseignant de mettre en place des cours de façon simple et conviviale pour des étudiants.

## Acteurs

### Réalisateurs

Ce projet a été réalisé par un groupe de dix étudiants de l'université d'Angers :
- Théo MAHAUDA : tmahauda@etud.univ-angers.fr ;
- Matthias LAUNAY : mlaunay@etud.univ-angers.fr ;
- Mohamed OUHIRRA : mouhirra@etud.univ-angers.fr ;
- Souleymane COULIBALY : scoulibaly@etud.univ-angers.fr ;
- Yassine HANINI : yhanini@etud.univ-angers.fr ;
- Toibrani OMAR : tomar@etud.univ-angers.fr ;
- Imad ARGANI : iargani@etud.univ-angers.fr ;
- Omar BOUBNINA : oboubnina@etud.univ-angers.fr ;
- Luca DAVIS : ldavis@etud.univ-angers.fr ;
- Andry RANDRIAMAMONJISOA : arandriamamonjisoa@etud.univ-angers.fr.

### Encadrants

Ce projet fut encadré par un enseignant de l'université d'Angers :
- Jean-Michel RICHER : jean-michel.richer@univ-angers.fr.

## Organisation

Ce projet a été agit au sein de l'université d'Angers dans le cadre du module "TER" du MASTER INFO 1.

## Date de réalisation

Ce projet a été éxécuté durant l'année 2020 sur la période du confinement COVID-19 à la maison au mois de Mars-Mai. 
Il a été terminé et rendu le 01/06/2020.

## Technologies, outils et procédés utilisés

Ce projet a été accomplis avec les technologies, outils et procédés suivants :
- Eclipse ;
- Java EE ;
- BDD MySQL ;
- Merise :
  - MCD ;
  - MLD ;
  - Scripts SQL ;
- Maven ;
- Docker ;
- Docker Compose ;
- Spring Boot :
  - Spring Data JPA ; 
  - Spring Web MVC (AOP, DAO, DTO, ...) ;
  - Spring Security (JWT, ...) ;
  - Spring Test ;
- API RESTfull (JSON, Swagger, ...)
- Angular :
  - Material Angular ;
  - Bootstrap ;
- Java FX ;
- JHipster.

## Objectifs

Jean Michel RICHER, enseignant à l’université d’Angers et commanditaire de l’application, veut un
logiciel qui doit permettre :
- Aux administrateurs du logiciel de gérer toutes les opérations mises en place pour les enseignants
et les étudiants, avec en plus des super fonctionnalités non accessibles pour ces deux types
d’utilisateurs, notamment pour gérer les utilisateurs inscrits et les structures présentes ;
- Aux enseignants de gérer l’ensemble de ses cours et d’associer des tâches à distribuer aux
étudiants ;
- Aux étudiants de suivre les cours qui leurs sont attribués et consulter le planning des tâches qu'ils
doivent réaliser (lire un cours, faire un TD, rendre le code associé au TD, répondre à un QCM,
etc.).