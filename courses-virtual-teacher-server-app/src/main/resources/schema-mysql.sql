DROP DATABASE IF EXISTS virtual_teacher_course;
CREATE DATABASE IF NOT EXISTS virtual_teacher_course DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

USE virtual_teacher_course;

START TRANSACTION;

SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;

DROP TABLE IF EXISTS vtc_user;
CREATE TABLE IF NOT EXISTS vtc_user (
    u_id VARCHAR(36) NOT NULL,
    u_role VARCHAR(7) NOT NULL,
    u_last_name VARCHAR(255) NOT NULL,
    u_first_name VARCHAR(255) NOT NULL,
    u_date_birth DATE NOT NULL, -- Format par défaut : AAAA-MM-JJ
    u_email VARCHAR(255) NOT NULL,
    u_phone VARCHAR(10) NOT NULL,
    u_address VARCHAR(255) NOT NULL,
    u_active BOOLEAN NOT NULL,
    u_username VARCHAR(255) NOT NULL,
    u_password VARCHAR(255) NOT NULL, -- Cryptage avec BCrypt
    CONSTRAINT pk_u_id PRIMARY KEY (u_id),
    CONSTRAINT uk_u_last_name UNIQUE (u_last_name),
    CONSTRAINT uk_u_email UNIQUE (u_email),
    CONSTRAINT uk_u_phone UNIQUE (u_phone),
    CONSTRAINT uk_u_username UNIQUE (u_username)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; -- Gestion des clés étrangères avec ce moteur

DROP TABLE IF EXISTS vtc_course;
CREATE TABLE IF NOT EXISTS vtc_course (
    c_id VARCHAR(36) NOT NULL,
    c_title VARCHAR(255) NOT NULL,
    c_description TEXT,
    c_date_created DATETIME NOT NULL, -- Format par défaut : AAAA-MM-JJ HH:MM:SS
    c_date_last_modified DATETIME NOT NULL, -- Format par défaut : AAAA-MM-JJ HH:MM:SS
    c_teacher_hold VARCHAR(36) NOT NULL,
    CONSTRAINT pk_c_id PRIMARY KEY (c_id),
    CONSTRAINT uk_c_title UNIQUE (c_title),
    CONSTRAINT fk_c_teacher_hold FOREIGN KEY (c_teacher_hold)
      REFERENCES vtc_user(u_id)
        ON UPDATE RESTRICT
        ON DELETE RESTRICT
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; -- Gestion des clés étrangères avec ce moteur

DROP TABLE IF EXISTS vtc_course_element;
CREATE TABLE IF NOT EXISTS vtc_course_element (
    ce_id VARCHAR(36) NOT NULL,
    ce_type VARCHAR(10) NOT NULL,
    ce_title VARCHAR(255) NOT NULL,
    ce_description TEXT,
    ce_content TEXT NOT NULL,
    ce_format VARCHAR(5) NOT NULL,
    ce_position SMALLINT UNSIGNED NOT NULL, -- SMALLINT = 2^16 = 65 535
    ce_expanded BOOLEAN NOT NULL,
    ce_selected BOOLEAN NOT NULL,
    ce_course_own VARCHAR(36),
    ce_over_element VARCHAR(36),
    CONSTRAINT pk_ce_id PRIMARY KEY (ce_id),
    CONSTRAINT uk_ce_title UNIQUE (ce_title),
    CONSTRAINT fk_ce_course_own FOREIGN KEY (ce_course_own)
      REFERENCES vtc_course(c_id)
        ON UPDATE RESTRICT
        ON DELETE RESTRICT,
    CONSTRAINT fk_ce_over_element FOREIGN KEY (ce_over_element)
      REFERENCES vtc_course_element(ce_id)
        ON UPDATE RESTRICT
        ON DELETE RESTRICT
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; -- Gestion des clés étrangères avec ce moteur

DROP TABLE IF EXISTS vtc_structure;
CREATE TABLE IF NOT EXISTS vtc_structure (
    s_id VARCHAR(36) NOT NULL,
    s_title VARCHAR(255) NOT NULL,
    s_description TEXT,
    s_url VARCHAR(255),
    s_position SMALLINT UNSIGNED NOT NULL, -- SMALLINT = 2^16 = 65 535
    s_expanded BOOLEAN NOT NULL,
    s_selected BOOLEAN NOT NULL,
    s_over_structure VARCHAR(36),
    CONSTRAINT pk_s_id PRIMARY KEY (s_id),
    CONSTRAINT uk_s_title UNIQUE (s_title),
    CONSTRAINT fk_s_over_structure FOREIGN KEY (s_over_structure)
      REFERENCES vtc_structure(s_id)
        ON UPDATE RESTRICT
        ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; -- Gestion des clés étrangères avec ce moteur

DROP TABLE IF EXISTS vtc_task;
CREATE TABLE IF NOT EXISTS vtc_task (
    t_id VARCHAR(36) NOT NULL,
    t_title VARCHAR(255) NOT NULL,
    t_description TEXT,
    t_date_begin DATETIME NOT NULL, -- Format par défaut : AAAA-MM-JJ HH:MM:SS
    t_date_end DATETIME NOT NULL, -- Format par défaut : AAAA-MM-JJ HH:MM:SS
    t_returned BOOLEAN NOT NULL,
    t_archive MEDIUMBLOB, -- Up to 16 Mb
    t_student_perform VARCHAR(36) NOT NULL,
    t_course_element_deal VARCHAR(36) NOT NULL,
    CONSTRAINT pk_t_id PRIMARY KEY (t_id),
    CONSTRAINT uk_t_title UNIQUE (t_title),
    CONSTRAINT fk_t_student_perform FOREIGN KEY (t_student_perform)
      REFERENCES vtc_user(u_id)
        ON UPDATE RESTRICT
        ON DELETE RESTRICT,
    CONSTRAINT fk_t_course_element_deal FOREIGN KEY (t_course_element_deal)
      REFERENCES vtc_course_element(ce_id)
        ON UPDATE RESTRICT
        ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; -- Gestion des clés étrangères avec ce moteur

DROP TABLE IF EXISTS vtc_email;
CREATE TABLE IF NOT EXISTS vtc_email (
    e_id VARCHAR(36) NOT NULL,
    e_subject VARCHAR(255) NOT NULL,
    e_body TEXT NOT NULL,
    e_date_send DATETIME NOT NULL, -- Format par défaut : AAAA-MM-JJ HH:MM:SS
    e_user_send VARCHAR(36) NOT NULL,
    e_course_element_concern VARCHAR(36),
    e_task_concern VARCHAR(36),
    CONSTRAINT pk_e_id PRIMARY KEY (e_id),
    CONSTRAINT uk_e_subject UNIQUE (e_subject),
    CONSTRAINT fk_e_user_send FOREIGN KEY (e_user_send)
      REFERENCES vtc_user(u_id)
        ON UPDATE RESTRICT
        ON DELETE RESTRICT,
    CONSTRAINT fk_e_course_element_concern FOREIGN KEY (e_course_element_concern)
      REFERENCES vtc_course_element(ce_id)
        ON UPDATE RESTRICT
        ON DELETE RESTRICT,
    CONSTRAINT fk_e_task_concern FOREIGN KEY (e_task_concern)
      REFERENCES vtc_task(t_id)
        ON UPDATE RESTRICT
        ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; -- Gestion des clés étrangères avec ce moteur

/*
CREATION DES TABLES ASSOCIATIONS POUR LES RELATIONS MANY TO MANY
*/

DROP TABLE IF EXISTS vtc_user_belong_structure;
CREATE TABLE IF NOT EXISTS vtc_user_belong_structure (
  ubs_user_id VARCHAR(36) NOT NULL,
  ubs_structure_id VARCHAR(36) NOT NULL,
  CONSTRAINT pk_ubs PRIMARY KEY (ubs_user_id, ubs_structure_id),
  CONSTRAINT fk_ubs_user_id FOREIGN KEY (ubs_user_id)
    REFERENCES vtc_user(u_id)
      ON UPDATE RESTRICT
      ON DELETE RESTRICT,
  CONSTRAINT fk_ubs_structure_id FOREIGN KEY (ubs_structure_id)
    REFERENCES vtc_user(u_id)
      ON UPDATE RESTRICT
      ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; -- Gestion des clés étrangères avec ce moteur

DROP TABLE IF EXISTS vtc_structure_study_course;
CREATE TABLE IF NOT EXISTS vtc_structure_study_course (
  ssc_structure_id VARCHAR(36) NOT NULL,
  ssc_course_id VARCHAR(36) NOT NULL,
  CONSTRAINT pk_ssc PRIMARY KEY (ssc_structure_id, ssc_course_id),
  CONSTRAINT fk_ssc_structure_id FOREIGN KEY (ssc_structure_id)
    REFERENCES vtc_structure(s_id)
      ON UPDATE RESTRICT
      ON DELETE RESTRICT,
  CONSTRAINT fk_ssc_course_id FOREIGN KEY (ssc_course_id)
    REFERENCES vtc_course(c_id)
      ON UPDATE RESTRICT
      ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; -- Gestion des clés étrangères avec ce moteur

DROP TABLE IF EXISTS vtc_user_receive_email;
CREATE TABLE IF NOT EXISTS vtc_user_receive_email (
  ure_user_id VARCHAR(36) NOT NULL,
  ure_email_id VARCHAR(36) NOT NULL,
  CONSTRAINT pk_ure PRIMARY KEY (ure_user_id, ure_email_id),
  CONSTRAINT fk_ure_user_id FOREIGN KEY (ure_user_id)
    REFERENCES vtc_user(u_id)
      ON UPDATE RESTRICT
      ON DELETE RESTRICT,
  CONSTRAINT fk_ure_email_id FOREIGN KEY (ure_email_id)
    REFERENCES vtc_email(e_id)
      ON UPDATE RESTRICT
      ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; -- Gestion des clés étrangères avec ce moteur

SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;

COMMIT;
