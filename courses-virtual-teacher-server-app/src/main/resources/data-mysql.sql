USE virtual_teacher_course;

START TRANSACTION;

SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;

/*
DATA USER
*/

-- ADMIN

-- login : radmin
-- password : Radmin49!
INSERT INTO vtc_user VALUES('03e18621-e174-49db-8300-b02a103dfe76', 'ADMIN', 'ADMIN', 'Root', '1960-01-01',
'root.admin@univ-angers.fr', '0240404040', '49000 Angers', 1, 'radmin', '$2y$10$eP0qg4Se3uDFHijuBdhfweq2z2o41kdRD6dcM2qNmBPP30.ntPi5W');

-- TEACHER

-- login : jmricher
-- password : JMricher49!
INSERT INTO vtc_user VALUES('dc322245-e177-4440-be42-d0ec51f1e246', 'TEACHER', 'RICHER', 'Jean-Michel', '1961-01-01',
'jean-michel.richer@univ-angers.fr', '0240404041', '49000 Angers', 1, 'jmricher', '$2y$10$OZVS7dQ972./HjyprWYd2O.oEuHhphjwbndNOwFM5j/9Ghd/9Mkxe');

-- login : ogoudet
-- password : Ogoudet49!
INSERT INTO vtc_user VALUES('7c70c82b-3348-48d9-8c7d-9b9ee64fcd60', 'TEACHER', 'GOUDET', 'Olivier', '1962-02-02',
'olivier.goudet@univ-angers.fr', '0240404042', '49000 Angers', 1, 'ogoudet', '$2y$10$3vsJQ05fslB8yOwvgPb18OhtgDIlNXTHyZyFtbi0PNKwEGI9hH1q6');

-- login : dgenest
-- password : Dgenest49!
INSERT INTO vtc_user VALUES('f69ae2b7-af66-446e-a9ef-fb6163a30a12', 'TEACHER', 'GENEST', 'David', '1963-03-03',
'david.genest@univ-angers.fr', '0240404043', '49000 Angers', 1, 'dgenest', '$2y$10$XmPpEDYqNqftkbS0Y0EpnO/Vb/FdtHS2xrWdchM1X3vkwgYmlXAba');

-- login : bdamota
-- password : Bdamota49!
INSERT INTO vtc_user VALUES('b8fc5955-cfcd-43b0-8521-2397d5a59b1e', 'TEACHER', 'DA MOTA', 'Benoît', '1964-04-04',
'benoit.da-mota@univ-angers.fr', '0240404044', '49000 Angers', 1, 'bdamota', '$2y$10$u0Rmnm0ZhyCfvPe0sLAN4.QYnoUbZQMn0mEm28tTSn3cvUrChhqPK');

-- STUDENT

-- login : tmahauda
-- password : Tmahauda49!
INSERT INTO vtc_user VALUES('06f51cf2-f943-4870-8072-e43d9b0df8d5', 'STUDENT', 'MAHAUDA', 'Théo', '1991-01-01',
'theo.mahauda@univ-angers.fr', '0240404141', '49000 Angers', 1, 'tmahauda', '$2y$10$GjpHZkr/KT0Tt7oRU7PZ8uHWsksxTpK5NTZGAwSpPLHj3N4pPX7Bq');

-- login : mlaunay
-- password : Mlaunauy49!
INSERT INTO vtc_user VALUES('c05ffa69-4303-4d13-99d4-98d1ed4dc58d', 'STUDENT', 'LAUNAY', 'Matthias', '1992-02-02',
'matthias.launay@univ-angers.fr', '0240404142', '49000 Angers', 1, 'mlaunay', '$2y$10$DtpyGLodrRGvCq8EBeLLsupLpOzZ6g.N.fk53m6nFuZgjYsMggoL2');

-- login : mouhirra
-- password : Mouhirra49!
INSERT INTO vtc_user VALUES('389e611b-e22a-4a41-bc96-4efba179d25d', 'STUDENT', 'OUHIRRA', 'Mohamed', '1993-03-03',
'mohamed.ouhirra@univ-angers.fr', '0240404143', '49000 Angers', 1, 'mouhirra', '$2y$10$DLvIjPD1oSQyAvE0WFtmyudyDA5CYg9s6a7LfZfVLkx/iXCn2EGPC');

-- login : scoulibaly
-- password : Scoulibaly49!
INSERT INTO vtc_user VALUES('cfedfca4-507d-49c5-ba0b-dc4fd58e6f34', 'STUDENT', 'COULIBALY', 'Souleymane', '1994-04-04',
'coulibaly.souleymane@univ-angers.fr', '0240404144', '49000 Angers', 1, 'scoulibaly', '$2y$10$RETcCW3/1XavTQAeduYXz.j1HDkHtEmAhYM97GS/M1kXO45FSBFkC');

-- login : yhanini
-- password : Yhanini49!
INSERT INTO vtc_user VALUES('495217ac-19e2-4a93-a50f-36be23dc0cc1', 'STUDENT', 'HANINI', 'Yassine', '1995-05-05',
'yassine.hanini@univ-angers.fr', '0240404145', '49000 Angers', 1, 'yhanini', '$2y$10$v5GCr6HfTZvbjNxa4J3e3ua0stikl6dQR.hBNc2OIZ9TzVazxTTNK');

-- login : tomar
-- password : Tomar49!
INSERT INTO vtc_user VALUES('1466efe4-c2a1-4257-838d-3f7cec79e698', 'STUDENT', 'OMAR', 'Toibrani', '1996-06-06',
'toibrani.omar@univ-angers.fr', '0240404146', '49000 Angers', 1, 'tomar', '$2y$10$2v0Fn8bCP5fkxv6d5YP7k.SCqvMRlOskJOmWlPw.d2vsQcR2l1oZu');

-- login : iargani
-- password : Iargani49!
INSERT INTO vtc_user VALUES('13a5d8de-aa7b-41a7-8a65-e8e895064480', 'STUDENT', 'ARGANI', 'Imad', '1997-07-07',
'imad.argani@univ-angers.fr', '0240404147', '49000 Angers', 1, 'iargani', '$2y$10$j4llw68NtgGvzFIrD8Lu/edGvVmwLjtR1xW4ZTkXX97MSSGpe5poe');

-- login : oboubnina
-- password : Oboubnina49!
INSERT INTO vtc_user VALUES('cc81f667-8e27-4690-9dad-113e5fdb4b4a', 'STUDENT', 'BOUBNINA', 'Omar', '1998-08-08',
'omar.boubnina@univ-angers.fr', '0240404148', '49000 Angers', 1, 'oboubnina', '$2y$10$a.CeUVrCcgcklNEDWGD3mOMlTTeM2okITsi9FbeMLXVGuQsVF9NhK');

-- login : ldavis
-- password : Ldavis49!
INSERT INTO vtc_user VALUES('2263044f-f30d-4377-bc7b-83a007aae52b', 'STUDENT', 'DAVIS', 'Luca', '1999-09-09',
'luca.davis@univ-angers.fr', '0240404149', '49000 Angers', 1, 'ldavis', '$2y$10$hs2pPrvuESIKwNshT5RNLuYHt2oGC1E3YAh18mfLvPaX41geiL96K');

-- login : arandria
-- password : Arandria49!
INSERT INTO vtc_user VALUES('4e0cb51e-ea75-4c8d-9c4d-fdd5635901ef', 'STUDENT', 'RANDRIA', 'Andry', '2000-10-10',
'andry.randria@univ-angers.fr', '0240404150', '49000 Angers', 1, 'arandria', '$2y$10$A4NyXV//q5MntZuhy2MM0uoIeDM9rRuysuwCjUQq6wwmKXMOfBMQe');

/*
DATA Structure
*/

INSERT INTO vtc_structure VALUES('98135393-ab03-4662-8613-0640968723ae', 'Université d\'Angers', NULL, 'https://www.univ-angers.fr/fr/index.html',
0, 0, 0, NULL);

INSERT INTO vtc_structure VALUES('c4ba52da-00c0-497a-8c6b-2d9ddbca3636', 'UFR Sciences', NULL, 'https://www.univ-angers.fr/fr/index.html',
0, 0, 0, '98135393-ab03-4662-8613-0640968723ae');

INSERT INTO vtc_structure VALUES('edd4079e-71f7-4f74-92ef-74126e447eff', 'Informatique', NULL, 'https://www.univ-angers.fr/fr/index.html',
0, 0, 0, 'c4ba52da-00c0-497a-8c6b-2d9ddbca3636');

INSERT INTO vtc_structure VALUES('e9c19109-3fb4-4729-a41e-3370d0857f42', 'L3 Informatique', NULL, 'https://www.univ-angers.fr/fr/index.html',
0, 0, 0, 'edd4079e-71f7-4f74-92ef-74126e447eff');

INSERT INTO vtc_structure VALUES('1ae688f1-deec-48c1-a198-a53f0bb8a864', 'L3 Informatique - Groupe 1', NULL, 'https://www.univ-angers.fr/fr/index.html',
0, 0, 0, 'e9c19109-3fb4-4729-a41e-3370d0857f42');

INSERT INTO vtc_structure VALUES('293e3a4f-4d07-4b9d-9b40-bc9f4fc74dae', 'L3 Informatique - Groupe 2', NULL, 'https://www.univ-angers.fr/fr/index.html',
0, 0, 0, 'e9c19109-3fb4-4729-a41e-3370d0857f42');

INSERT INTO vtc_structure VALUES('eafbf7dc-5bb9-4e63-9802-cd952c7e3a3f', 'M1 Informatique', NULL, 'https://www.univ-angers.fr/fr/index.html',
0, 0, 0, 'edd4079e-71f7-4f74-92ef-74126e447eff');

INSERT INTO vtc_structure VALUES('8139f013-d91d-4ce6-a35b-4b8da52cc629', 'M1 Informatique - Groupe 1', NULL, 'https://www.univ-angers.fr/fr/index.html',
0, 0, 0, 'eafbf7dc-5bb9-4e63-9802-cd952c7e3a3f');

INSERT INTO vtc_structure VALUES('42978801-00e8-4246-a4f0-a9073a9fca2e', 'M1 Informatique - Groupe 2', NULL, 'https://www.univ-angers.fr/fr/index.html',
0, 0, 0, 'eafbf7dc-5bb9-4e63-9802-cd952c7e3a3f');

/*
LINK User - Structure
*/

-- Tous les users sont inscrits dans la M1
INSERT INTO vtc_user_belong_structure VALUES('06f51cf2-f943-4870-8072-e43d9b0df8d5', 'eafbf7dc-5bb9-4e63-9802-cd952c7e3a3f');
INSERT INTO vtc_user_belong_structure VALUES('c05ffa69-4303-4d13-99d4-98d1ed4dc58d', 'eafbf7dc-5bb9-4e63-9802-cd952c7e3a3f');
INSERT INTO vtc_user_belong_structure VALUES('389e611b-e22a-4a41-bc96-4efba179d25d', 'eafbf7dc-5bb9-4e63-9802-cd952c7e3a3f');
INSERT INTO vtc_user_belong_structure VALUES('cfedfca4-507d-49c5-ba0b-dc4fd58e6f34', 'eafbf7dc-5bb9-4e63-9802-cd952c7e3a3f');
INSERT INTO vtc_user_belong_structure VALUES('495217ac-19e2-4a93-a50f-36be23dc0cc1', 'eafbf7dc-5bb9-4e63-9802-cd952c7e3a3f');
INSERT INTO vtc_user_belong_structure VALUES('1466efe4-c2a1-4257-838d-3f7cec79e698', 'eafbf7dc-5bb9-4e63-9802-cd952c7e3a3f');
INSERT INTO vtc_user_belong_structure VALUES('13a5d8de-aa7b-41a7-8a65-e8e895064480', 'eafbf7dc-5bb9-4e63-9802-cd952c7e3a3f');
INSERT INTO vtc_user_belong_structure VALUES('cc81f667-8e27-4690-9dad-113e5fdb4b4a', 'eafbf7dc-5bb9-4e63-9802-cd952c7e3a3f');
INSERT INTO vtc_user_belong_structure VALUES('2263044f-f30d-4377-bc7b-83a007aae52b', 'eafbf7dc-5bb9-4e63-9802-cd952c7e3a3f');
INSERT INTO vtc_user_belong_structure VALUES('4e0cb51e-ea75-4c8d-9c4d-fdd5635901ef', 'eafbf7dc-5bb9-4e63-9802-cd952c7e3a3f');

-- Affectation de 5 user dans le groupe 1 M1
INSERT INTO vtc_user_belong_structure VALUES('06f51cf2-f943-4870-8072-e43d9b0df8d5', '8139f013-d91d-4ce6-a35b-4b8da52cc629');
INSERT INTO vtc_user_belong_structure VALUES('c05ffa69-4303-4d13-99d4-98d1ed4dc58d', '8139f013-d91d-4ce6-a35b-4b8da52cc629');
INSERT INTO vtc_user_belong_structure VALUES('389e611b-e22a-4a41-bc96-4efba179d25d', '8139f013-d91d-4ce6-a35b-4b8da52cc629');
INSERT INTO vtc_user_belong_structure VALUES('cfedfca4-507d-49c5-ba0b-dc4fd58e6f34', '8139f013-d91d-4ce6-a35b-4b8da52cc629');
INSERT INTO vtc_user_belong_structure VALUES('495217ac-19e2-4a93-a50f-36be23dc0cc1', '8139f013-d91d-4ce6-a35b-4b8da52cc629');

-- Affectation de 5 user dans le groupe 2 M1
INSERT INTO vtc_user_belong_structure VALUES('1466efe4-c2a1-4257-838d-3f7cec79e698', '42978801-00e8-4246-a4f0-a9073a9fca2e');
INSERT INTO vtc_user_belong_structure VALUES('13a5d8de-aa7b-41a7-8a65-e8e895064480', '42978801-00e8-4246-a4f0-a9073a9fca2e');
INSERT INTO vtc_user_belong_structure VALUES('cc81f667-8e27-4690-9dad-113e5fdb4b4a', '42978801-00e8-4246-a4f0-a9073a9fca2e');
INSERT INTO vtc_user_belong_structure VALUES('2263044f-f30d-4377-bc7b-83a007aae52b', '42978801-00e8-4246-a4f0-a9073a9fca2e');
INSERT INTO vtc_user_belong_structure VALUES('4e0cb51e-ea75-4c8d-9c4d-fdd5635901ef', '42978801-00e8-4246-a4f0-a9073a9fca2e');

-- Affectation des teachers dans la structure Informatique
INSERT INTO vtc_user_belong_structure VALUES('dc322245-e177-4440-be42-d0ec51f1e246', 'edd4079e-71f7-4f74-92ef-74126e447eff');
INSERT INTO vtc_user_belong_structure VALUES('7c70c82b-3348-48d9-8c7d-9b9ee64fcd60', 'edd4079e-71f7-4f74-92ef-74126e447eff');
INSERT INTO vtc_user_belong_structure VALUES('f69ae2b7-af66-446e-a9ef-fb6163a30a12', 'edd4079e-71f7-4f74-92ef-74126e447eff');
INSERT INTO vtc_user_belong_structure VALUES('b8fc5955-cfcd-43b0-8521-2397d5a59b1e', 'edd4079e-71f7-4f74-92ef-74126e447eff');

/*
DATA Course
*/

-- Course IA

INSERT INTO vtc_course VALUES('04623f1d-d899-4a57-8962-e9696a51819a',
'Intelligence artificielle 1',
'Ce cours est la première partie du cours d’intelligence artificielle réparti sur les deux semestres. Le cours a pour objet de donner un large panorama des problématiques fondamentales de l’intelligence artificielle et d’étudier la représentation et la résolution de problèmes en IA utiles à la mise en œuvre d’un agent rationnel. Ce cours est en partie basé sur le livre Artificial Intelligence : A Modern Approach de Stuart Russell et Peter Norvig.
Certains enseignements fondamentaux de l’IA (comme la logique, web des données, ... ) font l’objet de cours spécifiques dans le cursus Licence-Master.',
'2001-01-01 11:11:11', '2011-01-01 11:11:11', 'dc322245-e177-4440-be42-d0ec51f1e246');

INSERT INTO vtc_course_element VALUES('5dde805d-c6f1-4497-a9d8-3f787808e645', 'SECTION',
'IA et Jeux : Méthodes de Résolution',
'Ce chapitre est une courte introduction aux jeux et aux principes de résolution de ces jeux avec IA.',
'IA et Jeux : Méthodes de Résolution',
'TEXTE', 0, 0, 0, '04623f1d-d899-4a57-8962-e9696a51819a', NULL);

  INSERT INTO vtc_course_element VALUES('675352b9-fa4b-4d04-850c-9352220a5ff9', 'SECTION',
  'Jeu de Nim',
  NULL,
  'Jeu de Nim',
  'TEXTE', 0, 0, 0, NULL, '5dde805d-c6f1-4497-a9d8-3f787808e645');

    INSERT INTO vtc_course_element VALUES('26c8411a-fcbf-4e8e-a532-778f6b53d6da', 'PARAGRAPHE',
    'Le Jeu de Nim est un jeu...',
    NULL,
    'Le Jeu de Nim est un jeu à deux joueurs qui consiste, étant données n
    allumettes, à prendre à tour de rôle 1, 2 ou 3 allumettes. Le perdant est le joueur qui prend la dernière allumette.',
    'TEXTE', 0, 0, 0, NULL, '675352b9-fa4b-4d04-850c-9352220a5ff9');

    INSERT INTO vtc_course_element VALUES('2dbc7177-707f-453e-b44a-cf4445744fcc', 'PARAGRAPHE',
    'Il s\'agit d\'un jeu dit...',
    NULL,
    '<div class="rounded_box_style1">
    <p>Il s\'agit d\'un jeu dit <em>à somme nulle</em>, c\'est à dire que la somme des gains et des pertes de tous les joueurs est égale à 0. Dans le cas présent il y a un gagnant et un perdant.</p>
    <p>Par opposition on parle de jeu à somme non nulle tel le <a href="https://fr.wikipedia.org/wiki/Dilemme_du_prisonnier">le dilemme du prisonnier</a>.</p>
    </div>',
    'HTML', 1, 0, 0, NULL, '675352b9-fa4b-4d04-850c-9352220a5ff9');

    INSERT INTO vtc_course_element VALUES('7b8a3d90-d2d9-4f0c-b4dc-684fc0815c70', 'IMAGE',
    'jeu_de_nim_situation.png',
    NULL,
    'http://info.univ-angers.fr/~richer/ens/m1/img/jeu_de_nim_situation.png',
    'TEXTE', 2, 0, 0, NULL, '675352b9-fa4b-4d04-850c-9352220a5ff9');

    INSERT INTO vtc_course_element VALUES('7e598974-2641-4014-ab7a-29c67e7611eb', 'PARAGRAPHE',
    'Pour 4 allumettes...',
    NULL,
    'Pour 4 allumettes :
    - si j\'en prends 3, alors je gagne car il ne reste qu\'une seule allumette et mon adversaire est contraint de la prendre
    - si j\'en prends 2, alors je perds car si mon adversaire en prend une seule, alors je serai contraint de prendre la dernière allumette
    - si j\'en prends 1, alors je perds car si mon adversaire en prend deux, alors je serai contraint de prendre la dernière allumette',
    'TEXTE', 3, 0, 0, NULL, '675352b9-fa4b-4d04-850c-9352220a5ff9');

    INSERT INTO vtc_course_element VALUES('d43ebf58-7fe4-4566-a57c-9ff84f89b828', 'PARAGRAPHE',
    'Jeu de Nim, situation Gagnante ou Perdante',
    NULL,
    '<div class="center">
      <table class="simple">
      <tr class="simple_header">
      	<td style="text-align: center">&nbsp;Allumettes<br/>restantes&nbsp;</td>
      	<td style="text-align: center">&nbsp;Allumette(s)<br/>prise(s)&nbsp;</td>
      	<td style="text-align: left">&nbsp;Situation&nbsp;</td>
      </tr>
      <tr>
      	<td style="text-align: center">&nbsp;1&nbsp;</td>
      	<td style="text-align: center">&nbsp;1&nbsp;</td>
      	<td style="text-align: left">&nbsp;P&nbsp;</td>
      </tr>
      <tr>
      	<td style="text-align: center">&nbsp;2&nbsp;</td>
      	<td style="text-align: center">&nbsp;1&nbsp;</td>
      	<td style="text-align: left">&nbsp;G&nbsp;</td>
      </tr>
      <tr>
      	<td style="text-align: center">&nbsp;3&nbsp;</td>
      	<td style="text-align: center">&nbsp;2&nbsp;</td>
      	<td style="text-align: left">&nbsp;G&nbsp;</td>
      </tr>
      <tr>
      	<td style="text-align: center">&nbsp;4&nbsp;</td>
      	<td style="text-align: center">&nbsp;3&nbsp;</td>
      	<td style="text-align: left">&nbsp;G&nbsp;</td>
      </tr>
      <tr>
      	<td style="text-align: center">&nbsp;5&nbsp;</td>
      	<td style="text-align: center">&nbsp;1,2,3&nbsp;</td>
      	<td style="text-align: left">&nbsp;P&nbsp;</td>
      </tr>
      <tr>
      	<td style="text-align: center">&nbsp;6&nbsp;</td>
      	<td style="text-align: center">&nbsp;1&nbsp;</td>
      	<td style="text-align: left">&nbsp;G - situation 5 pour adversaire&nbsp;</td>
      </tr>
      <tr>
      	<td style="text-align: center">&nbsp;6&nbsp;</td>
      	<td style="text-align: center">&nbsp;2&nbsp;</td>
      	<td style="text-align: left">&nbsp;P - situation 4 pour adversaire&nbsp;</td>
      </tr>
      <tr>
      	<td style="text-align: center">&nbsp;6&nbsp;</td>
      	<td style="text-align: center">&nbsp;3&nbsp;</td>
      	<td style="text-align: left">&nbsp;P - situation pour adversaire&nbsp;</td>
      </tr>
      <caption>Jeu de Nim, situation Gagnante ou Perdante</caption>
      </table>
    </div>',
    'HTML', 4, 0, 0, NULL, '675352b9-fa4b-4d04-850c-9352220a5ff9');

    INSERT INTO vtc_course_element VALUES('d9d9685e-d173-494d-8d2f-d9b49ace28de', 'PARAGRAPHE',
    'Au final...',
    NULL,
    '<div class="rounded_box_style1">
    <p>Au final, si $n = 4p + 1$ alors on perd.</p>
    <p>Il s\'agit donc d\'un jeu qui ne demande aucune IA pour gagner il suffit d\'appliquer la formule et faire en sorte que l\'adversaire soit dans une situation ou $n = 4p + 1$
    </div>',
    'HTML', 5, 0, 0, NULL, '675352b9-fa4b-4d04-850c-9352220a5ff9');

  INSERT INTO vtc_course_element VALUES('72ec053a-5bed-4280-8d1e-9389e473fd00', 'SECTION',
  'Jeu états / transitions',
  NULL,
  'Jeu états / transitions',
  'TEXTE', 1, 0, 0, NULL, '5dde805d-c6f1-4497-a9d8-3f787808e645');

  INSERT INTO vtc_course_element VALUES('efa2f98f-9eaf-484f-99f5-2cf064633529', 'SECTION',
  'Jeu avec IA de type MIN/MAX',
  NULL,
  'Jeu avec IA de type MIN/MAX',
  'TEXTE', 2, 0, 0, NULL, '5dde805d-c6f1-4497-a9d8-3f787808e645');

  INSERT INTO vtc_course_element VALUES('158d85ff-0b1b-4377-a52e-cff68c2353b4', 'SECTION',
  'Jeu de type Cryptarithme',
  NULL,
  'Jeu de type Cryptarithme',
  'TEXTE', 3, 0, 0, NULL, '5dde805d-c6f1-4497-a9d8-3f787808e645');

    INSERT INTO vtc_course_element VALUES('9cbc0497-b702-4665-b877-db09389814ab', 'SECTION',
    'Stratégies',
    NULL,
    'Stratégies',
    'TEXTE', 0, 0, 0, NULL, '158d85ff-0b1b-4377-a52e-cff68c2353b4');

      INSERT INTO vtc_course_element VALUES('3e749d90-7d88-45a6-a615-5f8e3f7cbfe3', 'PARAGRAPHE',
      'La résolution peut être...',
      NULL,
      'La résolution peut être plus ou moins longue en fonction de deux critères :
      l\'ordre dans lequel on traite les variables
      l\'ordre dans lequel on traite les valeurs à affecter aux variables
      Par exemple dans le cas de SEND + MORE = MONEY, il est préférable de commencer par
      M qui ne peut prendre que deux valeurs : 0 ou 1.',
      'TEXTE', 0, 0, 0, NULL, '9cbc0497-b702-4665-b877-db09389814ab');

      INSERT INTO vtc_course_element VALUES('343a8bef-a947-47e7-9824-6c4569b9796d', 'PARAGRAPHE',
      'Projet de programmation',
      NULL,
      '<div class="exercise"  exercise_id="1.3" ><p><b>Exercice 1.3</b><br /><p><b>Projet de programmation</b></p>

      <p>Ecrire un programme qui permet de résoudre les problèmes cryptarithmétiques étant donné les paramètres en ligne de commande :</p>

      <pre><code>
      \$ ./cryptarithme.exe SEND MORE MONEY
      S = ...
      E = ...

      \$ ./cryptarithme.exe UN UN NEUF ONZE
      U = ...
      </code></pre>

      <p>On définira les classes <b>Variable</b>, <b>Domain</b> et <b>Constraint</b> et on utilisera ces classes pour modéliser et résoudre le problème.</p>

      <p>Le projet est a réaliser seul ou en binôme et devra être rendu pour le <b class="red">10 Janvier 2020</b> par email sous forme d\'un fichier <b>.tgz</b> qui contiendra :</p>

      <ul>
      	<li>les fichiers sources</li>
      	<li>un makefile afin de compiler automatiquement votre code et générer un exécutable</li>
      	<li>éventuellement un README qui donnera des indications sur les spécificités de votre implantation</li>
      </ul>

      <p>le fichier .tgz devra être nommé suivant les noms des développeurs (par exemple <b>dupond_durand.tgz</b>) et devra lors du désarchivage créer un répertoire avec les noms des développeurs (<b>dupond_durand/...</b>).</p>

      </p></div>',
      'HTML', 1, 0, 0, NULL, '9cbc0497-b702-4665-b877-db09389814ab');

  -- Course DP

  INSERT INTO vtc_course VALUES('2ca375f7-3880-41b1-ae0a-d5eb9bf5918d',
  'Design patterns',
  'L\'objectif de ce cours est d\'apprendre les bonnes pratiques d\'utilisation des patrons de conception et leur mise en œuvre dans un langage objet.',
  '2002-02-02 12:12:12', '2012-02-02 12:12:12', '7c70c82b-3348-48d9-8c7d-9b9ee64fcd60');

  INSERT INTO vtc_course_element VALUES('f37e03c6-1665-4f8f-bfd3-f070d064bd61', 'SECTION',
  'Design patterns',
  'L\'objectif de ce cours est d\'apprendre les bonnes pratiques d\'utilisation des patrons de conception et leur mise en œuvre dans un langage objet.',
  'Design patterns',
  'TEXTE', 0, 0, 0, '2ca375f7-3880-41b1-ae0a-d5eb9bf5918d', NULL);

    INSERT INTO vtc_course_element VALUES('8064247e-6bcd-4fb8-9eb4-ee1d97d2e3ec', 'SECTION',
    'Introduction',
    NULL,
    'Introduction',
    'TEXTE', 0, 0, 0, NULL, 'f37e03c6-1665-4f8f-bfd3-f070d064bd61');

      INSERT INTO vtc_course_element VALUES('f2927a59-f7bd-4a5b-93d8-8b49ed110f22', 'PARAGRAPHE',
      'Qu’est-ce qu’un Design Pattern ?',
      NULL,
      'Design patterns (patrons de conception) : éléments de solution orienté-objet réutilisables pour résoudre des problèmes de programmation récurrents.
       Parution en 1994 du livre Design Patterns: Elements of Reusable Software co-écrit par quatre auteurs : Gamma, Helm, Johnson et Vlissides (Gang of Four - GoF).',
      'TEXTE', 0, 0, 0, NULL, '8064247e-6bcd-4fb8-9eb4-ee1d97d2e3ec');

      INSERT INTO vtc_course_element VALUES('585dd018-40a2-4652-8a49-77341e383899', 'PARAGRAPHE',
      'Ce n’est pas du code directement...',
      NULL,
      'Ce n’est pas du code directement, mais des façons d’organiser le code orienté-objet de façon à améliorer :
      - La flexibilité
      - La maintenance
      - L’extensibilité (faire des ajouts dans un programme en modifiant au minimum le code existant)
      Principe général : favorisez une conception modulaire du code. Voyez la programmation d’un logiciel comme un assemblage de briques substituables.',
      'TEXTE', 1, 0, 0, NULL, '8064247e-6bcd-4fb8-9eb4-ee1d97d2e3ec');

    INSERT INTO vtc_course_element VALUES('fbf03038-ba67-4272-bb9d-ad6737d6f6ad', 'SECTION',
    'Séance 1 - Rappels de Java',
    NULL,
    'Séance 1 - Rappels de Java',
    'TEXTE', 1, 0, 0, NULL, 'f37e03c6-1665-4f8f-bfd3-f070d064bd61');

    INSERT INTO vtc_course_element VALUES('0a6d62d7-23f3-4c58-b4aa-6db8c0354a93', 'SECTION',
    'Séance 2 - patterns Observateur et MVC',
    NULL,
    'Séance 2 - patterns Observateur et MVC',
    'TEXTE', 2, 0, 0, NULL, 'f37e03c6-1665-4f8f-bfd3-f070d064bd61');

      INSERT INTO vtc_course_element VALUES('d2213a7b-37fb-400d-9001-8913179f0447', 'SECTION',
      'Design pattern Observateur',
      NULL,
      'Design pattern Observateur',
      'TEXTE', 0, 0, 0, NULL, '0a6d62d7-23f3-4c58-b4aa-6db8c0354a93');

        INSERT INTO vtc_course_element VALUES('1d9f1592-e45c-4f4d-ae2e-3c10719334e0', 'PARAGRAPHE',
        'Définition Observateur',
        NULL,
        'Le pattern Observateur définit une relation entre objets de type un à plusieurs, de façon que, lorsque un objet change d’état, tous ceux qui en dépendent en soient notifiés et soient mis à jour automatiquement.',
        'TEXTE', 0, 0, 0, NULL, 'd2213a7b-37fb-400d-9001-8913179f0447');

      INSERT INTO vtc_course_element VALUES('68b71d42-14bc-492e-b43c-24ce3bf862b3', 'SECTION',
      'Design pattern MVC',
      NULL,
      'Design pattern MVC',
      'TEXTE', 1, 0, 0, NULL, '0a6d62d7-23f3-4c58-b4aa-6db8c0354a93');

        INSERT INTO vtc_course_element VALUES('1ad2d362-3eb4-4f4a-89fa-402ab4cdddd0', 'PARAGRAPHE',
        'Définition MVC',
        NULL,
        'Implémentation d’un MVC dans le projet Bomberman pour gérer les liens dynamiques entre la vue du jeu, l’interface utilisateur et l’état du jeu.',
        'TEXTE', 0, 0, 0, NULL, '68b71d42-14bc-492e-b43c-24ce3bf862b3');

  /*
  DATA Task
  */

  -- Task IA

  -- Théo
  INSERT INTO vtc_task VALUES('43891911-82c2-4a5b-abd8-e828daefcac2', 'Projet de programmation IA',
  'Ecrire un programme qui permet de résoudre les problèmes cryptarithmétiques',
  '2019-12-20 17:00:00', '2020-01-10 23:59:00', 0, NULL,
  '06f51cf2-f943-4870-8072-e43d9b0df8d5', '158d85ff-0b1b-4377-a52e-cff68c2353b4');

  -- Task DP

  -- Théo
  INSERT INTO vtc_task VALUES('ee4459ba-0f10-4c74-a52f-275df0bbb7c4', 'Projet Bomberman',
  'Vous devez implémenter le jeu Bomberman en Java avec les bonnes pratiques orienté objet',
  '2019-09-26 8:00:00', '2019-12-26 23:59:00', 0, NULL,
  '06f51cf2-f943-4870-8072-e43d9b0df8d5', 'f37e03c6-1665-4f8f-bfd3-f070d064bd61');

  /*
  LINK Structure - Course
  */
  INSERT INTO vtc_structure_study_course VALUES('eafbf7dc-5bb9-4e63-9802-cd952c7e3a3f', '04623f1d-d899-4a57-8962-e9696a51819a');
  INSERT INTO vtc_structure_study_course VALUES('eafbf7dc-5bb9-4e63-9802-cd952c7e3a3f', '2ca375f7-3880-41b1-ae0a-d5eb9bf5918d');

  /*
  DATA Email
  */

  -- JMR envoi un mail pour une précision sur la tache IA
  INSERT INTO vtc_email VALUES('3ee57bf0-fa10-4064-9978-e3a9ce505193', 'Précision sur la tâche IA',
  'Vous devez implémenter le programme en C++', '2020-01-01 11:11:11', 'dc322245-e177-4440-be42-d0ec51f1e246',
  NULL, '43891911-82c2-4a5b-abd8-e828daefcac2');

  -- L'étudiant Mohamed répond à JMR
  INSERT INTO vtc_email VALUES('7ac47d98-7f15-40a2-ae6a-e4e129660429', 'RE : Précision sur la tâche IA',
  'OK ca marche', '2020-01-01 18:18:18', '389e611b-e22a-4a41-bc96-4efba179d25d',
  NULL, '43891911-82c2-4a5b-abd8-e828daefcac2');

  /*
  LINK Structure - Course
  */

  -- Les 10 étudiants ont recus le mails

  INSERT INTO vtc_user_receive_email VALUES('06f51cf2-f943-4870-8072-e43d9b0df8d5', '3ee57bf0-fa10-4064-9978-e3a9ce505193');
  INSERT INTO vtc_user_receive_email VALUES('13a5d8de-aa7b-41a7-8a65-e8e895064480', '3ee57bf0-fa10-4064-9978-e3a9ce505193');
  INSERT INTO vtc_user_receive_email VALUES('1466efe4-c2a1-4257-838d-3f7cec79e698', '3ee57bf0-fa10-4064-9978-e3a9ce505193');
  INSERT INTO vtc_user_receive_email VALUES('2263044f-f30d-4377-bc7b-83a007aae52b', '3ee57bf0-fa10-4064-9978-e3a9ce505193');
  INSERT INTO vtc_user_receive_email VALUES('389e611b-e22a-4a41-bc96-4efba179d25d', '3ee57bf0-fa10-4064-9978-e3a9ce505193');
  INSERT INTO vtc_user_receive_email VALUES('495217ac-19e2-4a93-a50f-36be23dc0cc1', '3ee57bf0-fa10-4064-9978-e3a9ce505193');
  INSERT INTO vtc_user_receive_email VALUES('4e0cb51e-ea75-4c8d-9c4d-fdd5635901ef', '3ee57bf0-fa10-4064-9978-e3a9ce505193');
  INSERT INTO vtc_user_receive_email VALUES('c05ffa69-4303-4d13-99d4-98d1ed4dc58d', '3ee57bf0-fa10-4064-9978-e3a9ce505193');
  INSERT INTO vtc_user_receive_email VALUES('cc81f667-8e27-4690-9dad-113e5fdb4b4a', '3ee57bf0-fa10-4064-9978-e3a9ce505193');
  INSERT INTO vtc_user_receive_email VALUES('cfedfca4-507d-49c5-ba0b-dc4fd58e6f34', '3ee57bf0-fa10-4064-9978-e3a9ce505193');

  --JMR recoit la réponse de Mohamed
  INSERT INTO vtc_user_receive_email VALUES('dc322245-e177-4440-be42-d0ec51f1e246', '7ac47d98-7f15-40a2-ae6a-e4e129660429');

SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;

COMMIT;
