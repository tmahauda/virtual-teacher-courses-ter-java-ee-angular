package universite.angers.master.info.courses.virtual.teacher.server.app.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import universite.angers.master.info.courses.virtual.teacher.models.app.dto.course.CourseElementDTO;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.course.CourseElement;
import universite.angers.master.info.courses.virtual.teacher.server.app.mapper.CourseElementMapper;
import universite.angers.master.info.courses.virtual.teacher.server.app.security.SecurityConstants;
import universite.angers.master.info.courses.virtual.teacher.server.app.service.CourseElementService;
import java.util.Collection;

@Api("API pour les opérations CRUD sur les éléments de cours")
@RestController
@RequestMapping(value = SecurityConstants.COURSE_ELEMENT_URL)
public class CourseElementController implements EntityController<CourseElementDTO> {

	@Autowired
	private CourseElementService courseElementService;
	
	@Autowired
	private CourseElementMapper courseElementMapper;

	@ApiOperation(value = "Récupèrer la liste complète de tous les éléments de cours", authorizations = { @Authorization(value="jwtToken") })
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<Collection<CourseElementDTO>> readAll() {
		return ResponseEntity.ok(courseElementMapper.entitiesToDtos(courseElementService.readAll()));
	}

	@ApiOperation(value = "Récupère un élément de cours grâce à son id", authorizations = { @Authorization(value="jwtToken") })
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<CourseElementDTO> readById(@PathVariable String id) {
		return ResponseEntity.ok(courseElementMapper.entityToDto(courseElementService.readById(id)));
	}
	
	@ApiOperation(value = "Ajouter un élément de cours", authorizations = { @Authorization(value="jwtToken") })
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<CourseElementDTO> create(@RequestBody CourseElementDTO courseElementDTO) {
		CourseElement element = courseElementService.create(courseElementMapper.dtoToEntity(courseElementDTO));
		return ResponseEntity.status(HttpStatus.CREATED).body(courseElementMapper.entityToDto(element));
	}

	@ApiOperation(value = "Mettre à jour un élément de cours en fournissant un id et un élément de cours sous format json", authorizations = { @Authorization(value="jwtToken") })
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<CourseElementDTO> update(@PathVariable String id, @RequestBody CourseElementDTO courseElementDTO) {
		CourseElement elementOld = courseElementService.readById(id);
		CourseElement elemntNew = courseElementMapper.dtoToEntity(courseElementDTO);
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(courseElementMapper.entityToDto(courseElementService.update(elementOld)));
	}
	
	@ApiOperation(value = "Supprimer un élément de cours en fournissant un id", authorizations = { @Authorization(value="jwtToken") })
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<?> delete(@PathVariable String id) {
		CourseElement element = courseElementService.readById(id);
		courseElementService.delete(element);
		return ResponseEntity.status(HttpStatus.ACCEPTED).build();
	}
}