package universite.angers.master.info.courses.virtual.teacher.server.app.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import universite.angers.master.info.courses.virtual.teacher.models.app.dto.task.TaskDTO;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.task.Task;
import universite.angers.master.info.courses.virtual.teacher.server.app.mapper.TaskMapper;
import universite.angers.master.info.courses.virtual.teacher.server.app.security.SecurityConstants;
import universite.angers.master.info.courses.virtual.teacher.server.app.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Collection;

@Api("API pour les opérations CRUD sur les tâches")
@RestController
@RequestMapping(value = SecurityConstants.TASK_URL)
public class TaskController implements EntityController<TaskDTO> {

	@Autowired
	private TaskService taskService;
	
	@Autowired
	private TaskMapper taskMapper;

	@ApiOperation(value = "Récupèrer la liste complète de toutes les tâches que les étudiants doivent accomplir", authorizations = { @Authorization(value="jwtToken") })
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<Collection<TaskDTO>> readAll() {
		return ResponseEntity.ok(taskMapper.entitiesToDtos(taskService.readAll()));
	}

	@ApiOperation(value = "Récupère une tâche grâce à son id", authorizations = { @Authorization(value="jwtToken") })
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<TaskDTO> readById(@PathVariable String id) {
		return ResponseEntity.ok(taskMapper.entityToDto(taskService.readById(id)));
	}
	
	@ApiOperation(value = "Ajouter une tâche", authorizations = { @Authorization(value="jwtToken") })
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<TaskDTO> create(@RequestBody TaskDTO taskDTO) {
		Task task = taskService.create(taskMapper.dtoToEntity(taskDTO));
		return ResponseEntity.status(HttpStatus.CREATED).body(taskMapper.entityToDto(task));
	}

	@ApiOperation(value = "Mettre à jour une tâche en fournissant un id et une tâche sous format json", authorizations = { @Authorization(value="jwtToken") })
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<TaskDTO> update(@PathVariable String id, @RequestBody TaskDTO taskDTO) {
		Task taskOld = taskService.readById(id);
		Task taskNew = taskMapper.dtoToEntity(taskDTO);
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(taskMapper.entityToDto(taskService.update(taskOld)));
	}
	
	@ApiOperation(value = "Supprimer une tâche en fournissant un id", authorizations = { @Authorization(value="jwtToken") })
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<?> delete(@PathVariable String id) {
		Task task = taskService.readById(id);
		taskService.delete(task);
		return ResponseEntity.status(HttpStatus.ACCEPTED).build();
	}
}