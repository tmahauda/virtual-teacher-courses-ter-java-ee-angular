package universite.angers.master.info.courses.virtual.teacher.server.app.mapper;

import org.mapstruct.Mapper;
import universite.angers.master.info.courses.virtual.teacher.models.app.dto.course.CourseDTO;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.course.Course;

@Mapper(componentModel = "spring")
public interface CourseMapper extends EntityMapper<Course, CourseDTO> {

}
