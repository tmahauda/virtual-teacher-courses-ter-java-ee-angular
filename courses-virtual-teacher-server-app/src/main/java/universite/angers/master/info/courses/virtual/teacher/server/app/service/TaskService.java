package universite.angers.master.info.courses.virtual.teacher.server.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.task.Task;
import universite.angers.master.info.courses.virtual.teacher.server.app.dao.TaskDAO;
import universite.angers.master.info.courses.virtual.teacher.server.app.exceptions.ResourceNotFoundException;
import java.util.Collection;

@Service
public class TaskService implements EntityService<Task> {

	@Autowired
	private TaskDAO taskDAO;

	@Override
	public Task create(Task task) {
		return taskDAO.save(task);
	}

	@Override
	public Collection<Task> readAll() {
		return taskDAO.findAll();
	}

	@Override
	public Task readById(String id) {
		return taskDAO.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Task not found for this id : " + id));
	}

	@Override
	public Task update(Task task) {
		return taskDAO.save(task);
	}

	@Override
	public void delete(Task task) {
		taskDAO.delete(task);
	}
}