package universite.angers.master.info.courses.virtual.teacher.server.app.mapper;

import org.mapstruct.Mapper;
import universite.angers.master.info.courses.virtual.teacher.models.app.dto.task.TaskDTO;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.task.Task;

@Mapper(componentModel = "spring")
public interface TaskMapper extends EntityMapper<Task, TaskDTO> {

}
