package universite.angers.master.info.courses.virtual.teacher.server.app.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import universite.angers.master.info.courses.virtual.teacher.models.app.dto.course.CourseDTO;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.course.Course;
import universite.angers.master.info.courses.virtual.teacher.server.app.mapper.CourseMapper;
import universite.angers.master.info.courses.virtual.teacher.server.app.security.SecurityConstants;
import universite.angers.master.info.courses.virtual.teacher.server.app.service.CourseService;
import java.util.Collection;

@Api("API pour les opérations CRUD sur les cours")
@RestController
@RequestMapping(value = SecurityConstants.COURSE_URL)
public class CourseController implements EntityController<CourseDTO> {

	@Autowired
	private CourseService courseService;
	
	@Autowired
	private CourseMapper courseMapper;

	@ApiOperation(value = "Récupèrer la liste complète de tous les cours", authorizations = { @Authorization(value="jwtToken") })
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<Collection<CourseDTO>> readAll() {
		return ResponseEntity.ok(courseMapper.entitiesToDtos(courseService.readAll()));
	}

	@ApiOperation(value = "Récupèrer un cours grâce à son id", authorizations = { @Authorization(value="jwtToken") })
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<CourseDTO> readById(@PathVariable String id) {
		return ResponseEntity.ok(courseMapper.entityToDto(courseService.readById(id)));
	}
	
	@ApiOperation(value = "Ajouter un cours", authorizations = { @Authorization(value="jwtToken") })
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<CourseDTO> create(@RequestBody CourseDTO courseDTO) {
		Course course = courseService.create(courseMapper.dtoToEntity(courseDTO));
		return ResponseEntity.status(HttpStatus.CREATED).body(courseMapper.entityToDto(course));
	}

	@ApiOperation(value = "Mettre à jour un cours en fournissant un id et un cours sous format json", authorizations = { @Authorization(value="jwtToken") })
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<CourseDTO> update(@PathVariable String id, @RequestBody CourseDTO courseDTO) {
		Course courseOld = courseService.readById(id);
		Course courseNew = courseMapper.dtoToEntity(courseDTO);
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(courseMapper.entityToDto(courseService.update(courseOld)));
	}
	
	@ApiOperation(value = "Supprimer un cours en fournissant un id", authorizations = { @Authorization(value="jwtToken") })
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<?> delete(@PathVariable String id) {
		Course course = courseService.readById(id);
		courseService.delete(course);
		return ResponseEntity.status(HttpStatus.ACCEPTED).build();
	}
}