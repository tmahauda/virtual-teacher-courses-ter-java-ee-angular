package universite.angers.master.info.courses.virtual.teacher.server.app.mapper;

import org.mapstruct.Mapper;
import universite.angers.master.info.courses.virtual.teacher.models.app.dto.course.CourseElementDTO;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.course.CourseElement;

@Mapper(componentModel = "spring")
public interface CourseElementMapper extends EntityMapper<CourseElement, CourseElementDTO> {

}
