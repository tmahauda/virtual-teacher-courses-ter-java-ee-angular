package universite.angers.master.info.courses.virtual.teacher.server.app.service;

import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.course.Course;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.email.Email;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.structure.Structure;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.task.Task;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.user.User;
import universite.angers.master.info.courses.virtual.teacher.server.app.dao.UserDAO;
import universite.angers.master.info.courses.virtual.teacher.server.app.exceptions.ResourceNotFoundException;

@Service
public class UserService implements EntityService<User> {
	
	@Autowired
	private UserDAO userDao;

	@Override
	public User create(User user) {
		return userDao.save(user);
	}
	
	@Override
	public Collection<User> readAll() {
		return userDao.findAll();
	}
	
	public Collection<Structure> readAllStructuresBelong(String id) {
		User user = this.readById(id);
		return user.getStructuresBelong();
	}

	public Collection<Course> readAllCoursesHold(String id) {
		User user = this.readById(id);
		return user.getCoursesHold();
	}
	
	public Collection<Task> readAllTasksPerform(String id) {
		User user = this.readById(id);
		return user.getTasksPerform();
	}
	
	public Collection<Email> readAllEmailsSend(String id) {
		User user = this.readById(id);
		return user.getEmailsSend();
	}
	
	public Collection<Email> readAllEmailsReceive(String id) {
		User user = this.readById(id);
		return user.getEmailsReceive();
	}
	
	@Override
	public User readById(String id) {
		return userDao.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("User not found for this id : " + id));
	}
	
	public User readCurrentUser() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username;
		
		if (principal instanceof UserDetails) {
			username = ((UserDetails)principal).getUsername();
		} else {
			username = principal.toString();
		}
		
		return this.readByUsername(username);
	}
	
	public User readByUsername(String username) {
		return userDao.findByUsername(username)
				.orElseThrow(() -> new ResourceNotFoundException("User not found for this username : " + username));
	}

	@Override
	public User update(User user) {
		return userDao.save(user);
	}
	
	@Override
	public void delete(User user) {
		userDao.delete(user);
	}
}