package universite.angers.master.info.courses.virtual.teacher.server.app.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import universite.angers.master.info.courses.virtual.teacher.models.app.dto.structure.StructureDTO;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.structure.Structure;
import universite.angers.master.info.courses.virtual.teacher.server.app.mapper.StructureMapper;
import universite.angers.master.info.courses.virtual.teacher.server.app.security.SecurityConstants;
import universite.angers.master.info.courses.virtual.teacher.server.app.service.StructureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Collection;

@Api("API pour les opérations CRUD sur les structures")
@RestController
@RequestMapping(value = SecurityConstants.STRUCTURE_URL)
public class StructureController implements EntityController<StructureDTO> {

	@Autowired
	private StructureService structureService;
	
	@Autowired
	private StructureMapper structureMapper;

	@ApiOperation(value = "Récupèrer la liste complète de toutes les structures", authorizations = { @Authorization(value="jwtToken") })
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<Collection<StructureDTO>> readAll() {
		return ResponseEntity.ok(structureMapper.entitiesToDtos(structureService.readAll()));
	}

	@ApiOperation(value = "Récupère une structure grâce à son id", authorizations = { @Authorization(value="jwtToken") })
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<StructureDTO> readById(@PathVariable String id) {
		return ResponseEntity.ok(structureMapper.entityToDto(structureService.readById(id)));
	}
	
	@ApiOperation(value = "Ajouter une structure", authorizations = { @Authorization(value="jwtToken") })
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<StructureDTO> create(@RequestBody StructureDTO structureDTO) {
		Structure structure = structureService.create(structureMapper.dtoToEntity(structureDTO));
		return ResponseEntity.status(HttpStatus.CREATED).body(structureMapper.entityToDto(structure));
	}

	@ApiOperation(value = "Mettre à jour une structure en fournissant un id et une structure sous format json", authorizations = { @Authorization(value="jwtToken") })
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<StructureDTO> update(@PathVariable String id, @RequestBody StructureDTO structureDTO) {
		Structure structureOld = structureService.readById(id);
		Structure structureNew = structureMapper.dtoToEntity(structureDTO);
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(structureMapper.entityToDto(structureService.update(structureOld)));
	}
	
	@ApiOperation(value = "Supprimer une structure en fournissant un id", authorizations = { @Authorization(value="jwtToken") })
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<?> delete(@PathVariable String id) {
		Structure structure = structureService.readById(id);
		structureService.delete(structure);
		return ResponseEntity.status(HttpStatus.ACCEPTED).build();
	}
}