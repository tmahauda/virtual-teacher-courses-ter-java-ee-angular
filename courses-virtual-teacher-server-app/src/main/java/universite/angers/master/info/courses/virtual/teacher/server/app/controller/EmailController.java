package universite.angers.master.info.courses.virtual.teacher.server.app.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import universite.angers.master.info.courses.virtual.teacher.models.app.dto.email.EmailDTO;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.email.Email;
import universite.angers.master.info.courses.virtual.teacher.server.app.mapper.EmailMapper;
import universite.angers.master.info.courses.virtual.teacher.server.app.security.SecurityConstants;
import universite.angers.master.info.courses.virtual.teacher.server.app.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Collection;

@Api("API pour les opérations CRUD sur les emails")
@RestController
@RequestMapping(value = SecurityConstants.EMAIL_URL)
public class EmailController implements EntityController<EmailDTO> {

	@Autowired
	private EmailService emailService;
	
	@Autowired
	private EmailMapper emailMapper;

	@ApiOperation(value = "Récupèrer la liste complète de tous les emails", authorizations = { @Authorization(value="jwtToken") })
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<Collection<EmailDTO>> readAll() {
		return ResponseEntity.ok(emailMapper.entitiesToDtos(emailService.readAll()));
	}

	@ApiOperation(value = "Récupèrer un email grâce à son id", authorizations = { @Authorization(value="jwtToken") })
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<EmailDTO> readById(@PathVariable String id) {
		return ResponseEntity.ok(emailMapper.entityToDto(emailService.readById(id)));
	}
	
	@ApiOperation(value = "Ajouter un email", authorizations = { @Authorization(value="jwtToken") })
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<EmailDTO> create(@RequestBody EmailDTO emailDTO) {
		Email email = emailService.create(emailMapper.dtoToEntity(emailDTO));
		return ResponseEntity.status(HttpStatus.CREATED).body(emailMapper.entityToDto(email));
	}

	@ApiOperation(value = "Mettre à jour un email en fournissant un id et un email sous format json", authorizations = { @Authorization(value="jwtToken") })
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<EmailDTO> update(@PathVariable String id, @RequestBody EmailDTO emailDTO) {
		Email emailOld = emailService.readById(id);
		Email emailNew = emailMapper.dtoToEntity(emailDTO);
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(emailMapper.entityToDto(emailService.update(emailOld)));
	}
	
	@ApiOperation(value = "Supprimer un email en fournissant un id", authorizations = { @Authorization(value="jwtToken") })
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<?> delete(@PathVariable String id) {
		Email email = emailService.readById(id);
		emailService.delete(email);
		return ResponseEntity.status(HttpStatus.ACCEPTED).build();
	}
}