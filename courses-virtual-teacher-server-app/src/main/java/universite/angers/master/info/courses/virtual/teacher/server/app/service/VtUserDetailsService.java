package universite.angers.master.info.courses.virtual.teacher.server.app.service;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import universite.angers.master.info.courses.virtual.teacher.models.app.entity.user.User;
import universite.angers.master.info.courses.virtual.teacher.server.app.dao.UserDAO;

@Service
public class VtUserDetailsService implements UserDetailsService {
	
	@Autowired
	private UserDAO userRepository;

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
 		Optional<User> user = userRepository.findByUsername(userName);
 		user.orElseThrow(() -> new UsernameNotFoundException("Not found " + userName));
 		return user.map(VtUserDetails::new).get();
	}
}
