package universite.angers.master.info.courses.virtual.teacher.server.app.dao;

import java.util.Optional;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.user.Role;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.user.User;

public interface UserDAO extends JpaRepository<User, String> {
	
	/**
	 * Récupérer tous les utilisateur qui ont compte active ou désactivé
	 * @param active
	 * @return
	 */
	public Set<User> findByActive(boolean active);
	
	/**
	 * Récupérer tous les utilisateur pour un rôle donné
	 * @param role
	 * @return
	 */
	public Set<User> findByRole(Role role);
	
	/**
	 * Récupérer un utilisateur par son nom de famille
	 * @param lastname le nom de famille
	 * @return
	 */
	public Optional<User> findByLastname(String lastname);
	
	/**
	 * Récupérer un utilisateur par son adresse mail
	 * @param email l'adresse mail
	 * @return
	 */
	public Optional<User> findByEmail(String email);
	
	/**
	 * Récupérer un utilisateur par son numéro de téléphone
	 * @param phone le numéro de téléphone
	 * @return
	 */
	public Optional<User> findByPhone(String phone);
	
	/**
	 * Récupérer un utilisateur par son identifiant
	 * @param username l'identifiant
	 * @return
	 */
	public Optional<User> findByUsername(String username);

	/**
	 * Vérifier si le nom existe dans la BD
	 * @param lastname
	 * @return
	 */
	public Boolean existsByLastname(String lastname);
	
	/**
	 * Vérifier si l'adresse mail existe dans la BD
	 * @param email
	 * @return
	 */
	public Boolean existsByEmail(String email);
	
	/**
	 * Vérifier si le numéro de téléphone existe dans la BD
	 * @param phone
	 * @return
	 */
	public Boolean existsByPhone(String phone);
	
	/**
	 * Vérifier si l'identifiant existe dans la BD
	 * @param username
	 * @return
	 */
	public Boolean existsByUsername(String username);
}