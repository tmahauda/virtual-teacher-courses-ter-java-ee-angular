package universite.angers.master.info.courses.virtual.teacher.server.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.email.Email;
import universite.angers.master.info.courses.virtual.teacher.server.app.dao.EmailDAO;
import universite.angers.master.info.courses.virtual.teacher.server.app.exceptions.ResourceNotFoundException;
import java.util.Collection;

@Service
public class EmailService implements EntityService<Email> {

	@Autowired
	private EmailDAO emailDAO;

	@Override
	public Email create(Email email) {
		return emailDAO.save(email);
	}

	@Override
	public Collection<Email> readAll() {
		return emailDAO.findAll();
	}

	@Override
	public Email readById(String id) {
		return emailDAO.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Email not found for this id : " + id));
	}

	@Override
	public Email update(Email email) {
		return emailDAO.save(email);
	}

	@Override
	public void delete(Email email) {
		emailDAO.delete(email);
	}
}