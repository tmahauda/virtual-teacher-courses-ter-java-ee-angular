package universite.angers.master.info.courses.virtual.teacher.server.app.service;

import java.util.Collection;

public interface EntityService<T> {
	
    public T create(T entity);
	
    public Collection<T> readAll();
    
    public T readById(String id);
    
    public T update(T entity);

    public void delete(T entity);
}
