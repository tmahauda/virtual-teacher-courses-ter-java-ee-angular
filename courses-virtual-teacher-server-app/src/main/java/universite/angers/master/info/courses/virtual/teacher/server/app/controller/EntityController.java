package universite.angers.master.info.courses.virtual.teacher.server.app.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Collection;

public interface EntityController<T> {
	
	public ResponseEntity<T> create(@RequestBody T dao);
	
    public ResponseEntity<Collection<T>> readAll();

    public ResponseEntity<T> readById(@PathVariable String id);

    public ResponseEntity<T> update(@PathVariable String id, @RequestBody T dao);
    
    public ResponseEntity<?> delete(@PathVariable String id);
}
