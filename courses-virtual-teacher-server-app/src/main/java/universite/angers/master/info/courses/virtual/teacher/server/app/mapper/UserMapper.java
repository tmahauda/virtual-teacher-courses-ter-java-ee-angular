package universite.angers.master.info.courses.virtual.teacher.server.app.mapper;

import org.mapstruct.Mapper;
import universite.angers.master.info.courses.virtual.teacher.models.app.dto.user.UserDTO;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.user.User;

@Mapper(componentModel = "spring")
public interface UserMapper extends EntityMapper<User, UserDTO> {

}
