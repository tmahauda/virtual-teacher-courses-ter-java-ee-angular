package universite.angers.master.info.courses.virtual.teacher.models.app.dto.email;

import java.util.Date;
import java.util.UUID;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import universite.angers.master.info.courses.virtual.teacher.models.app.dto.course.CourseElementDTO;
import universite.angers.master.info.courses.virtual.teacher.models.app.dto.task.TaskDTO;
import universite.angers.master.info.courses.virtual.teacher.models.app.dto.user.UserDTO;

@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
public class EmailDTO {

	private String id;

	private String subject;

	private String body;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "Europe/Paris")
	private Date dateSend;

	private UserDTO userSend;

	private CourseElementDTO courseElementConcern;
	
	private TaskDTO taskConcern;
	
	public EmailDTO() {
		this.id = UUID.randomUUID().toString();
		this.dateSend = new Date();
	}
}
