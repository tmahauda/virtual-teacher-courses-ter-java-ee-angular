package universite.angers.master.info.courses.virtual.teacher.models.app.dto.course;

import java.util.HashSet;
import java.util.Set;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.course.FormatElementCourse;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.course.TypeElementCourse;

@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
public class CourseElementDTO {

	private String id;

	private TypeElementCourse type;

	private String title;

	private String description;

	private String content;

	private FormatElementCourse format;

	private int position;

	private boolean expanded;

	private boolean selected;

	private Set<CourseElementDTO> subElements;
	
	public CourseElementDTO() {
		this.subElements = new HashSet<>();
	}
}
