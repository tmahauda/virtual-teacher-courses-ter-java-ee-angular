package universite.angers.master.info.courses.virtual.teacher.models.app.entity.structure;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.course.Course;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.user.User;

@Entity(name = "vtc_structure")
@Table(
		name = "vtc_structure",
		uniqueConstraints = {
				@UniqueConstraint(columnNames = "s_title", name="uk_s_title"),
		}
)
@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
public class Structure implements Comparable<Structure> {

	@Id
	@Column(name = "s_id", nullable = false, insertable = true, updatable = false, 
		columnDefinition = "VARCHAR(36)", length = 36)
	private String id;

	@Column(name = "s_title", nullable = false, insertable = true, updatable = true, 
		columnDefinition = "VARCHAR(255)", length = 255)
	private String title;

	@Column(name = "s_description", nullable = true, insertable = true, updatable = true, 
		columnDefinition = "TEXT", length = 65535)
	private String description;
	
	@Column(name = "s_url", nullable = true, insertable = true, updatable = true, 
		columnDefinition = "VARCHAR(255)", length = 255)
	private String url;

	@Column(name = "s_position", nullable = false, insertable = true, updatable = true, 
		columnDefinition = "SMALLINT UNSIGNED", length = 65535)
	private int position;

	@Column(name = "s_expanded", nullable = false, insertable = true, updatable = true, 
		columnDefinition = "BIT", length = 1)
	private boolean expanded;

	@Column(name = "s_selected", nullable = false, insertable = true, updatable = true, 
		columnDefinition = "BIT", length = 1)
	private boolean selected;

	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH })
	@JoinColumn(name = "s_over_structure", nullable = true, insertable = true, updatable = true, columnDefinition = "VARCHAR(36)",
		foreignKey = @ForeignKey(name = "fk_s_over_structure"))
	private Structure overStructure;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="overStructure")
	private Set<Structure> subStructures;

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH })
	@JoinTable(
			name = "vtc_structure_study_course",
			joinColumns = @JoinColumn(name = "ssc_structure_id", nullable = false, insertable = true, updatable = true, columnDefinition = "VARCHAR(36)",
					foreignKey = @ForeignKey(name = "fk_ssc_structure_id")),
			inverseJoinColumns = @JoinColumn(name = "ssc_course_id", nullable = false, insertable = true, updatable = true, columnDefinition = "VARCHAR(36)",
					foreignKey = @ForeignKey(name = "fk_ssc_course_id")))
	private Set<Course> coursesStudy;

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH }, mappedBy = "structuresBelong")
	private Set<User> usersBelong;

	public Structure() {
		this.id = UUID.randomUUID().toString();
		this.subStructures = new HashSet<>();
		this.coursesStudy = new HashSet<>();
		this.usersBelong = new HashSet<>();
	}

	@Override
	public int compareTo(Structure e) {
		return Integer.compare(this.position, e.position);
	}
}
