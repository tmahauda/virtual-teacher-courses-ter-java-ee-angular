package universite.angers.master.info.courses.virtual.teacher.models.app.entity.email;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.course.CourseElement;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.task.Task;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.user.User;

@Entity(name = "vtc_email")
@Table(
		name = "vtc_email",
		uniqueConstraints = {
				@UniqueConstraint(columnNames = "e_subject", name="uk_e_subject"),
		}
)
@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
public class Email {

	@Id
	@Column(name = "e_id", nullable = false, insertable = true, updatable = false, 
		columnDefinition = "VARCHAR(36)", length = 36)
	private String id;
	
	@Column(name = "e_subject", nullable = false, insertable = true, updatable = true, 
		columnDefinition = "VARCHAR(255)", length = 255)
	private String subject;

	@Column(name = "e_body", nullable = false, insertable = true, updatable = true, 
		columnDefinition = "TEXT", length = 65535)
	private String body;
	
	@Temporal(TemporalType.TIME)
	@Column(name = "e_date_send", nullable = false, insertable = true, updatable = true, 
		columnDefinition = "DATETIME")
	private Date dateSend;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH })
	@JoinColumn(name = "e_user_send", nullable = false, insertable = true, updatable = false, columnDefinition = "VARCHAR(36)",
		foreignKey = @ForeignKey(name = "fk_e_user_send"))
	private User userSend;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH })
	@JoinColumn(name = "e_course_element_concern", nullable = true, insertable = true, updatable = true, columnDefinition = "VARCHAR(36)",
		foreignKey = @ForeignKey(name = "fk_e_course_element_concern"))
	private CourseElement courseElementConcern;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH })
	@JoinColumn(name = "e_task_concern", nullable = true, insertable = true, updatable = true, columnDefinition = "VARCHAR(36)",
		foreignKey = @ForeignKey(name = "fk_e_task_concern"))
	private Task taskConcern;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
		CascadeType.REFRESH }, mappedBy = "emailsReceive")
	private Set<User> usersReceive;
	
	public Email() {
		this.id = UUID.randomUUID().toString();
		this.dateSend = new Date();
		this.usersReceive = new HashSet<>();
	}
}
