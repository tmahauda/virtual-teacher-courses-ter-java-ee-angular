package universite.angers.master.info.courses.virtual.teacher.models.app.entity.course;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.email.Email;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.task.Task;

@Entity(name = "vtc_course_element")
@Table(
		name = "vtc_course_element",
		uniqueConstraints = {
				@UniqueConstraint(columnNames = "ce_title", name="uk_ce_title"),
		}
)
@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
public class CourseElement implements Comparable<CourseElement> {

	@Id
	@Column(name = "ce_id", nullable = false, insertable = true, updatable = false, 
		columnDefinition = "VARCHAR(36)", length = 36)
	private String id;

	@Enumerated(EnumType.STRING)
	@Column(name = "ce_type", nullable = false, insertable = true, updatable = true, 
		columnDefinition = "VARCHAR(10)", length = 10)
	private TypeElementCourse type;
	
	@Column(name = "ce_title", nullable = false, insertable = true, updatable = true, 
			columnDefinition = "VARCHAR(255)", length = 255)
	protected String title;

	@Column(name = "ce_description", nullable = true, insertable = true, updatable = true, 
			columnDefinition = "TEXT", length = 65535)
	protected String description;

	@Column(name = "ce_content", nullable = false, insertable = true, updatable = true, 
			columnDefinition = "TEXT", length = 65535)
	private String content;

	@Enumerated(EnumType.STRING)
	@Column(name = "ce_format", nullable = false, insertable = true, updatable = true, 
		columnDefinition = "VARCHAR(5)", length = 5)
	private FormatElementCourse format;
	
	@Column(name = "ce_position", nullable = false, insertable = true, updatable = true, 
			columnDefinition = "SMALLINT UNSIGNED", length = 65535)
	private int position;

	@Column(name = "ce_expanded", nullable = false, insertable = true, updatable = true, 
			columnDefinition = "BIT", length = 1)
	protected boolean expanded;

	@Column(name = "ce_selected", nullable = false, insertable = true, updatable = true, 
			columnDefinition = "BIT", length = 1)
	protected boolean selected;

	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
		CascadeType.REFRESH })
	@JoinColumn(name = "ce_course_own", nullable = false, insertable = true, updatable = true, columnDefinition = "VARCHAR(36)",
		foreignKey = @ForeignKey(name = "fk_ce_course_own"))
	private Course courseOwn;

	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH })
	@JoinColumn(name = "ce_over_element", nullable = true, insertable = true, updatable = true, columnDefinition = "VARCHAR(36)",
		foreignKey = @ForeignKey(name = "fk_ce_over_element"))
	private CourseElement overElement;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="overElement")
	private Set<CourseElement> subElements;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "courseElementDeal")
	private Set<Task> tasksDeal;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "courseElementConcern")
	private Set<Email> emailsConcern;
	
	public CourseElement() {
		this.id = UUID.randomUUID().toString();
		this.subElements = new HashSet<>();
	}

	@Override
	public int compareTo(CourseElement e) {
		return Integer.compare(this.position, e.position);
	}
}
