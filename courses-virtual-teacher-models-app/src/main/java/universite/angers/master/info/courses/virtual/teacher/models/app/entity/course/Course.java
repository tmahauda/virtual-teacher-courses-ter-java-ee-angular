package universite.angers.master.info.courses.virtual.teacher.models.app.entity.course;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.structure.Structure;
import universite.angers.master.info.courses.virtual.teacher.models.app.entity.user.User;

@Entity(name = "vtc_course")
@Table(
		name = "vtc_course",
		uniqueConstraints = {
				@UniqueConstraint(columnNames = "c_title", name="uk_c_title"),
		}
)
@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
public class Course {

	@Id
	@Column(name = "c_id", nullable = false, insertable = true, updatable = false, 
		columnDefinition = "VARCHAR(36)", length = 36)
	private String id;

	@Column(name = "c_title", nullable = false, insertable = true, updatable = true, 
			columnDefinition = "VARCHAR(255)", length = 255)
	private String title;

	@Column(name = "c_description", nullable = true, insertable = true, updatable = true, 
			columnDefinition = "TEXT", length = 65535)
	private String description;

	@Temporal(TemporalType.TIME)
	@Column(name = "c_date_created", nullable = false, insertable = true, updatable = true, 
		columnDefinition = "DATETIME")
	private Date dateCreated;

	@Temporal(TemporalType.TIME)
	@Column(name = "c_date_last_modified", nullable = false, insertable = true, updatable = true, 
		columnDefinition = "DATETIME")
	private Date dateLastModified;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
		CascadeType.REFRESH })
	@JoinColumn(name = "c_teacher_hold", nullable = false, insertable = true, updatable = true, columnDefinition = "VARCHAR(36)",
		foreignKey = @ForeignKey(name = "fk_teacher_hold"))
	private User teacherHold;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH }, mappedBy = "coursesStudy")
	private Set<Structure> structuresStudy;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "courseOwn")
	private Set<CourseElement> coursesElementsOwn;

	public Course() {
		this.id = UUID.randomUUID().toString();
		this.dateCreated = new Date();
		this.dateLastModified = new Date();
		this.coursesElementsOwn = new HashSet<>();
		this.structuresStudy = new HashSet<>();
	}
}