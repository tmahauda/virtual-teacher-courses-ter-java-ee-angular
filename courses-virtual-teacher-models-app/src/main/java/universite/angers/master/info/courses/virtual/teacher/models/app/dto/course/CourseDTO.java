package universite.angers.master.info.courses.virtual.teacher.models.app.dto.course;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import universite.angers.master.info.courses.virtual.teacher.models.app.dto.user.UserDTO;

@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
public class CourseDTO {

	private String id;

	private String title;

	private String description;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "Europe/Paris")
	private Date dateCreated;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "Europe/Paris")
	private Date dateLastModified;

	private UserDTO teacherHold;
	
	private Set<CourseElementDTO> coursesElementsOwn;

	public CourseDTO() {
		this.dateCreated = new Date();
		this.dateLastModified = new Date();
		this.coursesElementsOwn = new HashSet<>();
	}
}